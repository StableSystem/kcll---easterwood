# KCLL - Easterwood by Zero Dollar Payware

For brevity's sake and so I don't have to update multiple sources all information regarding this project is available on the official forum thread

**[OFFICIAL FORUM THREAD](https://forums.x-plane.org/index.php?/forums/topic/153488-kcll-easterwood-field-by-zero-dollar-payware/)**

**Required Libraries**
* MisterX Library
* RA Library
* SAM Library

To conctact me about this repo please send me a message on [the org](https://forums.x-plane.org/index.php?/profile/534962-stablesystem/)

To get the most up to date betas and alphas, download the pre-release versions from the official project repository. This repo also contains all of the source (including WED file) for the project if you want to make any modifications. For access to the source material used for custom assets check out the asset-source repo. 

https://gitlab.com/StableSystem/kcll---easterwood

https://gitlab.com/StableSystem/asset-source

This repository and its contents are protected under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)
Assets used from other developers was done so with their knowledge and approval
